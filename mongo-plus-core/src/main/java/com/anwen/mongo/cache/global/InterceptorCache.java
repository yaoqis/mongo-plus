package com.anwen.mongo.cache.global;

import com.anwen.mongo.interceptor.Interceptor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JiaChaoYang
 * @project mongo-plus
 * @description 拦截器
 * @date 2023-11-22 17:13
 **/
public class InterceptorCache {

    public static List<Interceptor> interceptors = new ArrayList<>();

}
